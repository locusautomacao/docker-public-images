# Docker Public Images

## Python 2.7.18-Slim with dependencies already installed

### Image name

```shell
locusdocker/python27:latest
```

### Directory

```shell
./python_2_7_base_image
```
