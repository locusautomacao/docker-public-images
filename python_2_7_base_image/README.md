## Python 2.7.18-Slim with dependencies already installed

### Image name

```shell
locusdocker/python27:latest
```

### Dependencies installed

```shell
timer-cm==1.1
beautifulsoup4==4.4.1
boto3==1.12.42
coreapi==2.3.3
cryptography==2.7
dj-database-url==0.3.0
dj-static==0.0.6
Django==1.11.18
django-common-helpers==0.7.0
django-cron==0.4.6
django-extensions==2.1.2
django-toolbelt==0.0.1
gunicorn==19.4.1
psycopg2==2.7.7
python-dateutil==2.4.2
pytz==2015.7
requests==2.8.1
six==1.11.0
static3==0.6.1
wheel==0.24.0
whitenoise==2.0.6
celery==3.1.19
django-celery==3.1.17
django-localflavor==1.3
hashids==1.2.0
django-session-security==2.6.0
django-simple-captcha==0.5.5
passlib==1.7.1
django-widget-tweaks==1.4.3
django-filter==1.0.4
djangorestframework==3.9.1
channels==1.1.8
msgpack==0.6.2
asgi-redis==1.4.3
markdown==3.0.1
model-mommy==1.6.0
aws-xray-sdk==1.1.2
git+git://github.com/rcaferraz/django-aws-xray@master#egg=django-aws-xray
django-webpack-loader==0.6.0
django-tenant-schemas==1.9.0
pika==1.1.0
Unidecode==1.1.1
sentry-sdk==0.14.2
mock==3.0.5
model-bakery==1.1.0
-e git+https://bitbucket.org/locusautomacao/metrics_cloudwatch@master#egg=metrics_cloudwatch
pika==1.1.0
ddtrace==0.37.0
func-timeout==4.3.5
python-memcached
```